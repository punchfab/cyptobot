<?php

class Process{
    private $pid;
    private $command;

    public function __construct($cl=false){
        if ($cl != false){
            $this->command = $cl;
            $this->runCom();
        }
    }
    private function runCom(){
        $command = 'nohup '.$this->command.' > /dev/null 2>&1 & echo $!';
        exec($command ,$op);
        $this->pid = (int)$op[0];
        // $this->pid = $op[0];
    }

    public function setPid($pid){
        $this->pid = $pid;
    }

    public function getPid(){
        return $this->pid;
    }

    public function status(){
        $command = 'ps -p '.$this->pid;
        exec($command,$op);
        if (!isset($op[1]))return false;
        else return true;
    }

    public function start(){
        if ($this->command != '')$this->runCom();
        else return true;
    }

    public function stop(){
        $command = 'kill '.$this->pid;
        // $command = 'sudo -u root -S kill '.$this->pid.' < /home/fixo/sups.secret';
        exec($command);
        if ($this->status() == false)return true;
        else return false;
    }
}

function pids($cmd, $option="verify", $arg="")
{
    // verify, restart, stop
    $pid = 0;
    $table = DB::table("process");
    $ps = $table->select("*", "cmd='$cmd' && status='1'");

    $c = count($ps);

    if ($c > 0) {
        //Existe el proceso y esta activo en la base de datos
        $pid = $ps[0]['pid'];
        $process = new Process();
        $process->setPid($pid);
        if (!$process->status()) {
            // El proceso no esta activo
            $table->update([
                "status" => (int)$process->status(),
            ], "id='".$ps[0]['id']."'");
            // Actualiza el estado
            if ($option != 'stop') {
                $process = new Process("$cmd $arg"); //Crea un nuevo proceso
                $pid = $process->getPid();

                $table->insert([
                    "pid" => $pid,
                    "cmd" => $cmd,
                    "status" => (int)$process->status()
                ]);
            }
        } else {
            if ($option != 'verify') {
                $process->stop();
                $table->update([
                    "status" => (int)$process->status(),
                ], "id='{$ps[0]['id']}'");
            }
            if ($option == 'restart') {
                $process = new Process("$cmd $arg");
                $pid = $process->getPid();
                $table->insert([
                    "pid" => $pid,
                    "cmd" => $cmd,
                    "status" => (int)$process->status()
                ]);
            }
        }
    } else {
        // No existe el proceso
        if ($option != 'stop') {
            $process = new Process("$cmd $arg");
            $pid = $process->getPid();

            // Crea un nuevo proceso
            $table->insert([
                "pid" => $pid,
                "cmd" => $cmd,
                "status" => (int)$process->status()
            ]);
        }
    }
    deleteDuplicate($cmd, $option);
    return "status:$c - pid:$pid\n";
}


function deleteDuplicate($cmd, $option = 'verify') {
    $ps = DB::table("process")->select("*", "cmd='$cmd' && status='1'","order by id desc");
    $c = 0;
    foreach ($ps as $pss) {
        if (($c > 0 && $option != 'stop') || $option == 'stop') {
            $process = new Process();
            $process->setPid($pss['pid']);
            $process->stop();
            DB::table("process")->update([
                "status" => (int)$process->status(),
            ], "id='".$pss['id']."'");
        }
        // echo $c.PHP_EOL;
        $c++;
    }
}
?>