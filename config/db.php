<?php
/**
 * review, started, finished, canceled
 */
require 'general.php';

class DB
{
	private $dbh;
	private $table;

	function __construct($table = "")
	{
		$this->table = $table;
		try{
        	// Conexión a la base de datos
		    $this->dbh = new PDO('mysql:host='.DBHOST.';dbname='.DBBASE, DBUSER, DBPASS);
		    $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}catch(PDOException $e){
		    echo "ERROR: " . $e->getMessage();
		}

	}

	public function table($name)
	{
		return new DB($name);
	}

	public static function run_sql($sql)
	{
		static $mysqli;

		$f = true;
		$g = $x = false;
		$res = array();

		if (!$mysqli)
		{
			$dbhost = DBHOST;
			$port = NULL; $socket = NULL; 
			if (strpos('localhost', ":"))
			{ 
				list($dbhost, $port) = explode(':', 'localhost');
				if (is_numeric($port)) $port = (int) $port;
				else
				{
					$socket = $port;
					$port = NULL;
				}
			}
			$mysqli = @mysqli_connect($dbhost, DBUSER, DBPASS, DBBASE, $port, $socket);
			if (mysqli_connect_errno())
			{
				echo "<br /><b>Error. Can't connect to your MySQL server.</b> You need to have PHP 5.2+ and MySQL 5.5+ with mysqli extension activated. <a href='http://crybit.com/how-to-enable-mysqli-extension-on-web-server/'>Instruction &#187;</a>\n";
				echo "<br />Also <b>please check DB username/password in file</b>\n";
				die("<br />Server has returned error - <b>".mysqli_connect_error()."</b>");
			}
			$mysqli->query("SET NAMES utf8");
		}

		$query = $mysqli->query($sql);

		if ($query === FALSE) die("MySQL Error: ".$mysqli->error."; SQL: $sql");
		elseif (is_object($query) && $query->num_rows)
		{
			while($row = $query->fetch_object())
			{
				if ($f)
				{
					if (property_exists($row, "idx")) $x = true;
					$c = count(get_object_vars($row));
					if ($c > 2 || ($c == 2 && !$x)) $g = true;
					elseif (!property_exists($row, "nme")) die("Error in run_sql() - 'nme' not exists! SQL: $sql");
					$f = false;
				}

				if (!$g && $query->num_rows == 1 && property_exists($row, "nme")) return $row->nme;
				elseif ($x) $res[$row->idx] = ($g) ? $row : $row->nme;
				else $res[] = ($g) ? $row : $row->nme;
			}
		}
		elseif (stripos($sql, "insert ") !== false) $res = $mysqli->insert_id;

		if (is_object($query)) $query->close();
		if (is_array($res) && count($res) == 1 && isset($res[0]) && is_object($res[0])) $res = $res[0];

		return $res;
	}

	public function insert($arg = [])
	{
		$date = new DateTime();
		$date->setTimezone(new DateTimeZone('America/Guayaquil'));
		$dt =  $date->format('Y-m-d H:i:s');

		if(empty($this->table)) return 'Error - table no selected';

		$table = $this->table;
		$columns = implode(", ", array_keys($arg));
		$values = implode(", :", array_keys($arg));
		try
 		{
	 		$sql = "INSERT INTO $table ($columns, created_at) VALUES (:$values, :created_at)";
	 		$stmt = $this->dbh->prepare($sql);

	 		foreach ($arg as $c => $v) $stmt->bindParam(":$c", $arg[$c]);
	 		$stmt->bindParam(":created_at", $dt);
	 		$stmt->execute();
	 		// $result = $stmt->fetch(PDO::FETCH_ASSOC);
	 		return $this->find($this->dbh->lastInsertId());
	 		// return $result['id'];
	 		// echo "registro creado exito";
 		}catch(PDOException $e){
 			echo $e->getMessage();
 		}
	}

	public function select($columns, $where = "", $condition = "")
	{
		$table = $this->table;
		try {
			if (!empty($where)) $where = "WHERE ".$where;
	 		$sql = "SELECT $columns FROM $table $where $condition";
	 		$stmt = $this->dbh->prepare($sql);
	 		$stmt->execute();
	 		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			echo $e->getMessage();
		}


	}

	public function update($arg = [], $where = "", $condition = "")
 	{
 		$date = new DateTime();
		$date->setTimezone(new DateTimeZone('America/Guayaquil'));
		$dt =  $date->format('Y-m-d H:i:s');

 		$table = $this->table;
		$columns = implode(" = ?, ", array_keys($arg)) . " = ?, updated_at = ?";
		// $values = implode(", :", array_keys($arg));
 		try{
	 		if(!empty($where))
	 		{
	 			$where = " WHERE " . $where;
	 		}
	 		$sql = "UPDATE $table SET $columns $where $condition";
	 		$stmt = $this->dbh->prepare($sql);

	 		$c = 1;
	 		foreach ($arg as $col => $val) {
	 			$stmt->bindParam($c, $arg[$col]);
	 			$c++;
	 		}

	 		$stmt->bindParam($c, $dt);
	 		$stmt->execute();
 		}catch(PDOException $e){
 			echo $e->getMessage();
 		}
 	}

 	public function delete($where = "", $condition = "")
 	{
 		// $where = explode("=", $where);
 		// return $where;
 		$table = $this->table;
 		try{
	 		if(!empty($where)) $where = "WHERE " . $where;
	 		$sql = "DELETE FROM $table $where $condition";
	 		$stmt = $this->dbh->prepare($sql);
	 		$stmt->execute();
	 	}catch(PDOException $e){
	 		echo $e->getMessage();
	 	}
 	}

 	public function find($id)
 	{
 		$table = $this->table;
		try {
	 		$sql = "SELECT * FROM $table WHERE id='$id'";
	 		$stmt = $this->dbh->prepare($sql);
	 		$stmt->execute();
	 		return $stmt->fetch(PDO::FETCH_ASSOC);
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
 	}
}
?>