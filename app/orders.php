<?php
require_once __DIR__ . '/../vendor/autoload.php';
use Carbon\Carbon;

class Order
{
	private $api;
	private $symbol;
	private $coin;
	private $dataBuy = [];
	private $dataSell = [];
	
	function __construct($symbol = "", $data = [])
	{
		$this->api = new Binance\API(API_KEY,SECRET);
		$this->symbol = $symbol;
		$this->coin = explode("BTC", $symbol)[0];

		if (array_key_exists('orderBuy', $data) && $data['orderBuy']['status'] != 'FILLED')
			$this->dataBuy = $data['orderBuy'];
		if (array_key_exists('orderSell', $data) && $data['orderSell']['status'] != 'FILLED')
			$this->dataSell = $data['orderSell'];
	}

	public function buy($price = "", $type = "limit")
	{
		$symbol = $this->symbol;
		$digits = intval(DB::table("symbols")->select("digits","name='$symbol'")[0]['digits']);
		$quantity = 0;
		$openorders = $this->api->openOrders($symbol);
		// if ($quantity >= 1) $quantity = intval($quantity);
		// else $quantity = truncateF($quantity, 3);
		if (array_key_exists('orderId', $this->dataBuy)) {
			$response = $this->api->cancel($symbol, $this->dataBuy['orderId']);
		} 
		elseif (count($openorders) > 0 && !array_key_exists('code', $openorders)) {
			if ($openorders[0]['side'] == 'BUY') {
				$orderid = $openorders[0]['orderId'];
				$response = $this->api->cancel($symbol, $orderid);
			}
		}
		$balances = $this->api->balances();
		$quantity = truncateF($balances['BTC']['available'] / $price, $digits);
		if ($balances['BTC']['available'] >= 0.001) {
			switch ($type) {
				case 'limit':
					$order = $this->api->buy($this->symbol, $quantity, $price);
					break;
				
				case 'market':
					$order = $this->api->marketBuy($this->symbol, $quantity);
					break;

				default:
					$order = ['code' => 'Error', 'msg' => 'No existe este tipo de orden'];
					break;
			}
			$order['digits'] = $digits;
			$order['quantity'] = $quantity;
			$order['type'] = $type;
			$order['price'] = $price;
			return $order;
		}
		return ["code" => "Error", "msg" => "No hay suficientes fondos", "digits" => $digits, "quantity" => $quantity, "type" => $type, "price" => $price];
	}

	public function sell($price = "", $type = "stop_limit", $quantity = NULL, $stopPrice = "")
	{
		$symbol = $this->symbol;
		$digits = intval(DB::table("symbols")->select("digits","name='$symbol'")[0]['digits']);
		$openorders = $this->api->openOrders($symbol);
		if (array_key_exists('orderId', $this->dataSell)) {
			$orderStatus = $this->api->orderStatus($symbol, $this->dataSell['orderId']);
			if ($orderStatus['status'] != 'CANCELED') {
				$response = $this->api->cancel($symbol, $orderStatus);
				if (is_null($quantity)) $quantity = truncateF($orderStatus['origQty'], $digits);
			}
		} elseif (count($openorders) > 0 && !array_key_exists('code', $openorders)) {
			if ($openorders[0]['side'] == 'SELL') {
				$orderid = $openorders[0]['orderId'];
				$quantity = truncateF($openorders[0]['origQty'], $digits);
				$response = $this->api->cancel($symbol, $orderid);
			}
		}
		$balances = $this->api->balances($this->api->prices());

		if (is_null($quantity)) {
			if ($balances[$this->coin]['available'] > 0.00000000) {
				$quantity = truncateF($balances[$this->coin]['available'], $digits);
			}elseif($balances[$this->coin]['onOrder'] > 0.00000000) {
				$quantity = truncateF($balances[$this->coin]['onOrder'], $digits);
			}
		}
		
		$total = $quantity * $price;

		if ($total >= 0.001) {
			switch ($type) {
				case 'limit':
					$order = $this->api->sell($symbol, $quantity, $price);
					if (array_key_exists('msg', $order) && $order['msg'] == 'Filter failure: PRICE_FILTER') {
						$price = truncateF($price,7);
						$order = $this->api->sell($symbol, $quantity, $price);
					}
					break;
				
				case 'stop_limit':
					$stopPrice = $stopPrice == "" ? $price : $stopPrice;
					$order = $this->api->sell($symbol, $quantity, $price, "STOP_LOSS_LIMIT", ["stopPrice"=>$stopPrice]);
					if (array_key_exists('msg', $order) && $order['msg'] == 'Filter failure: PRICE_FILTER') {
						$price = truncateF($price,7);
						$stopPrice = truncateF($stopPrice,7);
						$order = $this->api->sell($symbol, $quantity, $price, "STOP_LOSS_LIMIT", ["stopPrice"=>$stopPrice]);
					}
					break;

				case 'market':
					$order = $this->api->marketSell($symbol, $quantity);
					break;
				default:
					$order = ['code' => 'Error', 'msg' => 'No existe este tipo de orden'];
					break;
			}

			$order['digits'] = $digits;
			$order['quantity'] = $quantity;
			$order['type'] = $type;
			$order['price'] = $price;
			$order['total'] = $total;
			return $order;
		}
		return ["code" => "Error", "msg" => "No hay suficientes fondos", "digits" => $digits, "quantity" => $quantity, "type" => $type, "price" => $price, "total" => $total];
	}

	public function getBalances($option = "all")
	{
		$balances = $this->api->balances();
		$balances[$this->symbol];

		foreach ($balances as $key => $value) {
			if ($balances[$key]['available'] > 0.00000000) {
				echo "$key\n";
				echo "available:\t".$balances[$key]['available']."\n";
				echo "available:\t".$balances[$key]['available']."\n";
				echo "onOrder:\t".$balances[$key]['onOrder']."\n";
				echo "btcValue:\t".$balances[$key]['btcValue']."\n";
				echo "btcTotal:\t".$balances[$key]['btcTotal']."\n\n";
			}
		}
	}

	public function btcAvailable() {
		$balances = $this->api->balances();
		if ($balances['BTC']['available'] >= 0.001) return 1;
		return 0;
	}
}

function truncateF($number, $digitos)
{
	$r = 10;
	$multiplicador = pow ($r,$digitos);
	$resultado = ((int)($number * $multiplicador)) / $multiplicador;
	return number_format($resultado, $digitos, '.', '');
}

function send_notification($data = array())
{
	//$msg["sound"] = "default";
	$access_key = "AAAA7Sl6biA:APA91bHe0SI4ZsXhbA6s8moT9--d3TPlvndWEV2EBP9Z4Ka-uaIpnjSOQZsSSs1wJ7on2Mo1E74Ys6nNKWvJmd68WRp65RxrCuWZ4A_NisBPFd2XTj6hfpIjIN2h6Xim9T5uibJiS6uEgZM654UCxMKtkcJj8s1fVQ";
	$token = "/topics/cyptobot";
    
    /*$msg = array
          (
		"body" 	=> "El usuario ".$this->usuario." envio comprobante de pago",
		"title"	=> "Verificacion de Orden",
        "sound" => "default"
          );
  	
    
    $data = array
          (
		"orden"	=> $this->orden,
		"descripcion"	=> $this->orden . "descripcion",
		"url_img" => $this->img
          );*/
  	
	$fields = array
			(
				'to'		=> $token,
				'data'	=> $data
			);
	
	
	$headers = array
			(
				'Authorization: key=' . $access_key,
				'Content-Type: application/json'
			);
	#Send Reponse To FireBase Server
	
	$ch = curl_init();
	curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	curl_setopt( $ch,CURLOPT_POST, true );
	curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	$result = curl_exec($ch );
	curl_close( $ch );
	echo $result;
}