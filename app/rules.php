<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../indicators.php';
require __DIR__ . '/orders.php';

use Carbon\Carbon;

$api =  new Binance\API(API_KEY,SECRET);
$spi =  new Binance\API(API_KEY,SECRET);

$coin_review = DB::table("coin_review")->select("*","json_extract(rules, '$.status') = '1' && status!='finished' && status='canceled'");

$api->chart(array_column($coin_review, "symbol"), "5m", function($api, $symbol, $chart) {
	global $coin_review;
	$kcoin = [];
	$reviewActived = (boolean) DB::table("settings")->find(1)['value'];
	if (!$reviewActived)) exit();
	$result = 0;
	
	foreach ($coin_review as $coin) {
		if ($coin['symbol'] == $symbol) {
			$kcoin = DB::table("coin_review")->find($coin['id']);
		}
	}

	$rules = json_decode($kcoin['rules'], true);
	foreach ($rules['conditions'] as $condition) {
		switch ($condition['indicator']) {
			case 'one ema':
				$one_ema = one_ema($spi, $symbol, "4h", $alarm['vals']);
				$rule = strtolower($condition['rule']);
				switch ($rule) {
					case 'below':
						if ($one_ema['status'] == 0) $result = 1;
						break;
					case 'above':
						if ($one_ema['status'] == 1) $result = 1;
						break;
				}
				break;
			
			case 'double ema':
				$select_rule = ["Crosses Up" => 1 ,"New Crosses Up" => 2,"Crosses Down" => 0];
				$de = double_ema($chart);
				if ($de == $select_rule[$condition['rule']]) $result = 1;
				break;

			case 'rsi':
				$rule = strtolower($condition['rule']);
				$rsi = rsi($chart, $condition['values'], $rule);
				$result = $rsi['status'];
				break;

			case 'stochastic':
				$select_rule = [
					"Crosses Below 20" => "1is1", 
					"Crosses Above 80" => "1is0", 
					"Above 80" => "2is1", 
					"Below 80" => "2is0",
                    "Falling Line Above 80" => "3is1"
                ];
                $stoch_rule = explode("is", $select_rule[$condition['rule']]);
				$stoch = stochastic($chart, $stoch_rule[0]);
				if ($stoch['status'] == $stoch_rule[1]) $result = 1;
				break;

			case 'parabolic sar':
				break;
			default:
				# code...
				break;
		}
	}

});
?>