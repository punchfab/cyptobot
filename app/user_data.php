<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../indicators.php';
require __DIR__ . '/orders.php';
require __DIR__ . '/../config/Process.php';

use Carbon\Carbon;

$api =  new Binance\API(API_KEY,SECRET);
$balance_update = function($api, $balances) {
	print_r($balances['BTC']);
	echo "Balance update".PHP_EOL;
};

$order_update = function($api, $report) {
	pids('php /var/www/cyptobot/app/review.php','stop');
	echo "Order update".PHP_EOL;
	print_r($report);
	$rprice = $report['price'];
	$quantity = $report['quantity'];
	$symbol = $report['symbol'];
	$side = $report['side'];
	$orderType = $report['orderType'];
	$orderId = $report['orderId'];
	$orderStatus = $report['orderStatus'];
	$executionType = $report['orderStatus'];
	
	//NEW, CANCELED, REPLACED, REJECTED, TRADE, EXPIRED
	echo "{$symbol} {$side} {$executionType} {$orderType} ORDER #{$orderId}".PHP_EOL;
	
	$table = DB::table("coin_review");
	$coin = $table->select("*","(status='review' OR status='started') AND symbol='{$report['symbol']}'");
	$apiPrice = $api->prices();
	$balances = $api->balances($apiPrice);
	$execute = 0;


	if (count($coin) > 0) {
		$tCoin = $coin[0];
		$status = $tCoin['status'];
		$data = json_decode($tCoin['data'], true);
		$per = json_decode($tCoin['percentage'], true);
		$price = json_decode($tCoin['price'], true);

		$order = $api->orderStatus($report['symbol'], $report['orderId']);

		if ($report['side'] == 'BUY') {
			$data['orderBuy'] = $order;
			if ($order['status'] == 'FILLED' && $status == 'review') {
				// if (0.00000000
				$price['entry'] = $order['price'];
				$price['entryTime'] = Carbon::now('UTC')->timestamp;
				$status = 'started';
				send_notification([
					"id" => "{$tCoin['id']}",
					"title"	=> "Operación iniciada: $symbol",
					"body" 	=> "(Filled) Nueva operación iniciada con {$order['price']} BTC",
					"status" => "$status"
				]);
			}
			$execute = 1;
		}
		elseif ($report['side'] == 'SELL') {
			$data['orderSell'] = $order;

			if ($order['status'] == 'FILLED' && $status == 'started') {
				$status = 'finished';
				$price['exit'] = $order['price'];
				$price['exitTime'] = Carbon::now('UTC')->timestamp;
				$per['exit'] = percentage($price['entry'], $price['exit']);
				$data['balances']['final'] = [
					'btc_available' => $balances['BTC']['available'],
					'btc_total' => $api->btc_value
				];
				send_notification([
					"id" => "{$tCoin['id']}",
					"title"	=> "Operación finalizada: $symbol",
					"body" 	=> "(Filled) Finalizada con ".truncateF($per['exit'],2)."%",
					"status" => "$status"
				]);
			}
			$execute = 1;
		}
		// if ($execute) {

			$table->update([
				"status" => $status,
				"price" => json_encode($price),
				"percentage" => json_encode($per),
				"data" => json_encode($data)
			],"id='{$tCoin['id']}'");

	 		pids('php /var/www/cyptobot/app/alarms.php','restart');
			echo "Coin:".count($coin)." Updated".PHP_EOL;
		// }
	} else {
		if ($report['side'] == 'BUY') {
			$order = $api->orderStatus($report['symbol'], $report['orderId']);
			if ($order['status'] == 'FILLED') {
				$price = [
					"entry" => $order['price'],
					"entryTime" => $order['updateTime']/1000,
					"last" => $order['price'],
					"review" => $order['price'],
					"high" => $order['price'],
					"low" => $order['price']
				];
				$percentage = ['review' => 0, 'stop_loss' => 0, 'last_price' => 0, 'exit' => 0, 'high' => 0, 'low' => 0];
				$data = [
					"amount" => $order['origQty'],
					"orderBuy" => $order,
					"balances" => [
						"start" => [
							"btc_available" => sprintf('%.8f', $order['cummulativeQuoteQty'] + $balances['BTC']['available']),
							"btc_total" => $api->btc_value
						]
					]
				];

				$coinInsert = $table->insert([
					"symbol" => $order['symbol'],
					"status" => "started",
					"priority" => 1,
					"confirm" => "{}",
					"price" => json_encode($price),
					"percentage" => json_encode($percentage),
					"data" => json_encode($data),
				]);

				send_notification([
					"id" => $coinInsert['id'],
					"title"	=> "Operación iniciada: $symbol (Manual)",
					"body" 	=> "Nueva operación iniciada con {$price['entry']} BTC",
					"status" => "started"
				]);
			}
		}
	}
	
	$id = count($coin) > 0 ? $coin[0]['id'] : '0';
	
	if ( $executionType == "NEW" ) {
		$body =  "{$symbol} {$side} {$orderType} ORDER #{$orderId} ({$orderStatus})";
		if ( $executionType == "REJECTED" ) {
			$body = "Order Failed! Reason: {$report['rejectReason']}";
			echo $body.PHP_EOL;
		}
		send_notification([
			"id" => $id,
			"title"	=> "Nueva Orden: $symbol",
			"body" 	=> $body, 
			"status" => "change"
		]);
		
		echo "{$symbol} {$side} {$orderType} ORDER #{$orderId} ({$orderStatus})".PHP_EOL;
		echo "..price: {$rprice}, quantity: {$quantity}".PHP_EOL;
	}

	if ($executionType == "CANCELED" || $executionType == "REJECTED" || $executionType == "EXPIRED") {
		send_notification([
			"id" => $id,
			"title"	=> "Cambio De Estado: $symbol",
			"body" 	=> "$side $executionType $orderType $rprice - $orderId",
			"status" => "change"
		]);
	}
	pids('php /var/www/cyptobot/app/review.php','restart');
};
$api->userData($balance_update, $order_update);
?>