<?php 
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../indicators.php';
require __DIR__ . '/orders.php';

use Carbon\Carbon;

$api =  new Binance\API(API_KEY,SECRET);
$spi =  new Binance\API(API_KEY,SECRET);

$alarms = DB::table("alarms")->select("*", "active='1'");

$cAll = 0;
$cNew = 0;
$alarmCoins = [];
$review = DB::table("coin_review")->select("*", "status='started'");

foreach (array_column($alarms, "coin") as $coin) {
	if ($coin == 'all') {
		$cAll++;
	} else {
		array_push($alarmCoins, $coin);
		$cNew++;
	}
}

if ($cAll > 0) {
	foreach (array_column($review, "symbol") as $coin) {
		array_push($alarmCoins, $coin);
	}
}

$alarmCoins = array_unique($alarmCoins);
$api->chart($alarmCoins, "5m", function($api, $symbol, $chart) {
	$alarmActived = DB::table("settings")->find(2);
	if (!boolval($alarmActived['value'])) exit();
	global $alarms;
	global $review;
	$kcoin = [];
	$close = end($chart)['close'];
	$textInd = [
		"one_ema" => "One Ema",
	 	"double_ema" => "Double Ema",
	 	"rsi" => "RSI", 
	 	"stochastic" => "Stochastic", 
	 	"sar" => "Parabolic Sar",
        "percentage" => "Percentage"
    ];
	
	foreach ($review as $coin) {
		if ($coin['symbol'] == $symbol) {
			$kcoin = DB::table("coin_review")->find($coin['id']);
			// $kcoin = $coin;
		}
	}

	$textRule = ["below" => "Below","above" => "Above"];
	foreach ($alarms as $alarm) {
		$result = 0;
		switch ($alarm['indicator']) {
			case 'rsi':
				// rsi status params(chart, value, option)
				$textRule = ["below" => "Below","above" => "Above","cross" => "Crosses","prev" => "Crosses Up"];
				$rsi = rsi($chart, $alarm['vals'], $alarm['rule']);
				$result = $rsi['status'];
				break;
			case 'stochastic':
				// sthocastic status params(chart, option)
				$textRule = [
					"1is1" => "Crosses Below 20", 
					"1is0" => "Crosses Above 80", 
					"2is1" => "Above 80", 
					"2is0" => "Below 80",
                    "3is1" => "Falling Line Above 80"
                ];
				$rule = explode("is", $alarm['rule']);
				$stoch = stochastic($chart, $rule[0]);
				if ($stoch['status'] == $rule[1]) $result = 1;
				break;
			case 'one_ema':
				// one ema status params(api,symbol,interval,timePeriod)
				$one_ema = one_ema($spi, $symbol, "4h", $alarm['vals']);

				switch ($alarm['rule']) {
					case 'below':
						if ($one_ema['status'] == 0) $result = 1;
						break;
					case 'above':
						if ($one_ema['status'] == 1) $result = 1;
						break;
				}
				break;
			case 'percentage':
				// percentage
				if ($alarm["coin"] == "all") {
					$price = json_decode($kcoin['price'], true);
					$per = percentage($price['entry'], $close);
					$alarm['vals'] = $alarm['vals']." = ".number_format($per,2)."%";
					switch($alarm['rule']) {
						case 'below':
							if ($per <= $alarm['vals']) $result = 1;
							break;
						case 'above':
							if ($per >= $alarm['vals']) $result = 1;
							break;
					}
				}
				break;
			default:
				// sar status params(chart)
				// double ema params(chart)
				$textRule = ["1" => "Crosses Up","2" => "New Crosses Up","0" => "Crosses Down"];
				if ($alarm['indicator'] == 'sar') $textRule = ["1" => "Lower At Close","0" => "Greater At Close"];
				$indicator = call_user_func($alarm['indicator'], $chart);
				$status = $indicator;
				if (array_key_exists('status', $indicator)) $status = $indicator['status'];
				if ($status == $alarm['rule']) $result = 1;
				break;
		}
		// alert mobile
		$alarm['vals'] = (!isset($alarm['vals']) && empty($alarm['vals'])) ? "" : $alarm['vals'];

		if ($result == 1 && $kcoin['status'] == 'started') {
			if (count($kcoin) > 0 && $alarm["coin"] == "all") {
				send_notification([
					"id" => $kcoin['id'],
					"title"	=> "Alerta: $symbol",
					"body" 	=> $textInd[$alarm['indicator']]." ".$textRule[$alarm['rule']]." ".$alarm['vals'],
					"status" => "alert"
				]);
			} else {
				send_notification([
					"id" => 0,
					"title"	=> "Alerta: $symbol",
					"body" 	=> $textInd[$alarm['indicator']]." ".$textRule[$alarm['rule']]." ".$alarm['vals'],
					"status" => "alert"
				]);
			}
			echo $symbol."--".$textInd[$alarm['indicator']]." ".$textRule[$alarm['rule']]." ".$alarm['vals']."\n";
		}
		
	}
});
?>