<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/orders.php';

use Carbon\Carbon;

$api =  new Binance\API(API_KEY,SECRET);
$longopts = array(
	"activity:",
);
$options = getopt("", $longopts);

$api->ticker("BTCUSDT", function($api, $symbol, $ticker) {
	global $options;
	$dbBalances = DB::table("balances");
	$dbSymbols = DB::table("symbols");

	$tbBalances = $dbBalances->select("*","","ORDER BY id DESC LIMIT 1");
	$tbSymbols = $dbSymbols->select("*");
	$prices = $api->prices();
	$balances = $api->balances($prices);
	
	$usd_available = number_format($balances['BTC']['available'] * $ticker['close'],2);
	$usd_total = number_format($api->btc_value * $ticker['close'],2);
	$close = number_format($ticker['close'],2);
	echo "Price of BTC: $close USDT.".PHP_EOL;
	echo "Estimated Value: ".$api->btc_value." BTC".PHP_EOL;
	echo "USDT Value: $usd_total USD".PHP_EOL;
	echo "\n";
	if (count($tbBalances) > 0) {
		$tbBalances = $tbBalances[0];
		if ($tbBalances['btc_available'] == $balances['BTC']['available']) {
			if ($tbBalances['btc_total'] != $api->btc_value || $tbBalances['usd_available'] != $usd_available || $tbBalances['usd_total'] != $usd_total || $tbBalances['price'] != $close) {
				// Update
				$dbBalances->update([
					"btc_total" => $api->btc_value,
					"usd_available" => $usd_available,
					"usd_total" => $usd_total,
					"price" => $close
				],"id='{$tbBalances['id']}'");
			}
		} else {
			newBalances($dbBalances, $balances['BTC']['available'], $api->btc_value, $usd_available, $usd_total, $close);
		}
	} else {
		newBalances($dbBalances, $balances['BTC']['available'], $api->btc_value, $usd_available, $usd_total, $close);
	}
	$symbols = array_column($tbSymbols, 'name');
	foreach ($prices as $key => $value) {
		$offset = strlen($key) - 3;
		if (strpos($key, 'BTC', $offset) !== false && strpos($key, 'USD') === false && !in_array($key, $symbols)) {
			$dbSymbols->insert([
				"name" => $key,
				"price" => $value
			]);
		}
	}
	foreach ($tbSymbols as $symbols) {
		if ($prices[$symbols['name']] != $symbols['price']) {
			$dbSymbols->update(["price" => $prices[$symbols['name']]],"id='{$symbols['id']}'");
		}
	}
	if (intval($options['activity']) == 1) {
		send_notification([
			"activity" => "home"
		]);
	}
});

function newBalances($db, $btc_available, $btc_total, $usd_available, $usd_total, $price) {
	$db->insert([
			"btc_available" => $btc_available,
			"btc_total" => $btc_total,
			"usd_available" => $usd_available,
			"usd_total" => $usd_total,
			"price" => $price
		]);
}
/*$balance_update = function($api, $balances) {
	print_r($balances);
	echo "Balance update".PHP_EOL;
};

$order_update = function($api, $report) {
	echo "Order update".PHP_EOL;
	print_r($report);
	$price = $report['price'];
	$quantity = $report['quantity'];
	$symbol = $report['symbol'];
	$side = $report['side'];
	$orderType = $report['orderType'];
	$orderId = $report['orderId'];
	$orderStatus = $report['orderStatus'];
	$executionType = $report['orderStatus'];
	if ( $executionType == "NEW" ) {
		if ( $executionType == "REJECTED" ) {
			echo "Order Failed! Reason: {$report['rejectReason']}".PHP_EOL;
		}
		echo "{$symbol} {$side} {$orderType} ORDER #{$orderId} ({$orderStatus})".PHP_EOL;
		echo "..price: {$price}, quantity: {$quantity}".PHP_EOL;
		return;
	}
	//NEW, CANCELED, REPLACED, REJECTED, TRADE, EXPIRED
	echo "{$symbol} {$side} {$executionType} {$orderType} ORDER #{$orderId}".PHP_EOL;
};
$api->userData($balance_update, $order_update);
*/
?>