<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../indicators.php';
require __DIR__ . '/orders.php';
require __DIR__ . '/../config/Process.php';

use Carbon\Carbon;

$longopts = array(
	"activity:",
	"id::",
);
$options = getopt("", $longopts);
$api =  new Binance\API(API_KEY,SECRET);
$spi =  new Binance\API(API_KEY,SECRET);
$coins = DB::table("coin_review")->select("*", "status!='finished' && status!='canceled'", "limit 20");

$api->chart(array_column($coins, "symbol"), "5m", function($api, $symbol, $chart) {
	$reviewActived = (boolean) DB::table("settings")->find(1)['value'];
	$alarmActived = (boolean) DB::table("settings")->find(2)['value'];
	$testActive = (boolean) DB::table("settings")->find(4)['value'];
	if (!$reviewActived && !$testActive) exit();
	$startTime = microtime(true);
	global $coins;
	global $spi;
	global $options;
	$active = (boolean) DB::table("settings")->find(3)['value'];
	$kcoin = [];
	$sar = sar($chart);
 	$data = $msg = NULL;
	foreach ($coins as $coin) {
		if ($coin['symbol'] == $symbol) {
			$kcoin = DB::table("coin_review")->find($coin['id']);
		}
	}

	$close = end($chart)['close'];
	$confirm = json_decode($kcoin['confirm'], true);
	$data = json_decode($kcoin['data'],true);
	$msg = json_decode($kcoin['msg'],true);
	$price = json_decode($kcoin['price'],true);
	$per = json_decode($kcoin['percentage'], true);
 	$order = new Order($symbol, $data);
	
	$price['last'] = $close;
	$price['high'] = $price['last'] > $price['high'] ? $price['last'] : $price['high'];
	$price['low'] = $price['last'] < $price['low'] ? $price['last'] : $price['low'];
	$per['review'] = percentage($price['review'], $price['last']);
	// 1.REVIEW
	if ($kcoin['status'] == 'review') {
		$per['last_price'] = $per['review'];
		$per['high'] = percentage($price['review'], $price['high']);
		$per['low'] = percentage($price['review'], $price['low']);
		$status = 'without';
		$stop_loss = 0;
		// $per = ['stop_loss' => 0, 'last_price' => 0, 'exit' => 0];
		DB::table("coin_review")->update([
				"price" => json_encode($price),
				"percentage" => json_encode($per)
			], "id='{$kcoin['id']}'");
		// Evaluar para ingresar
		if ($active) {
			$kstatus = $kcoin['status'];
			if (array_key_exists('orderBuy', $data)) {
				$data['orderBuy'] = $api->orderStatus($symbol, $data['orderBuy']['orderId']);
				if ($data['orderBuy']['status'] == "FILLED") {
					$kstatus = "started";
					$price['entry'] = $data['orderBuy']['price'];
					$price['entryTime'] = Carbon::now('UTC')->timestamp;

					send_notification([
						"id" => "{$kcoin['id']}",
						"title"	=> "Operación iniciada: $symbol",
						"body" 	=> "Nueva operación iniciada con {$price['entry']} BTC",
						"status" => "started"
					]);
				}
				DB::table("coin_review")->update([
					"status" => $kstatus,
					"price" => json_encode($price),
					"percentage" => json_encode($per),
					"data" => json_encode($data),
				], "id='{$kcoin['id']}'");

				if ($alarmActived) {
			 		$cmd = "php /var/www/cyptobot/app/alarms.php";
			 		pids($cmd,'restart');
				}
				if ($kstatus == "started") return false;
			}
		}
		// 1. Si double ema es la primera en encontrarse
		if (array_key_exists('double-ema', $confirm) && $confirm['double-ema']['position'] == '1'){
			// Todavia sigue alcista, sar esta por debajo y rsi por debajo de 70
			$de = double_ema($chart);
			$ticks = $spi->candlesticks($symbol, "5m",12);
			$oe = one_ma($ticks, 1, "assetVolume")['status'];
			// $rsi_70 = rsi($chart, 70);&& $sar['status'] == 1 
		 	if ($de == 2 && $oe == 1 /*&& $rsi_70['status'] == 1*/) {
			 	// Entrar y actualizar la base de datos
			 	$status = "started";
		 	} elseif (double_ema($chart) == 0) {
			 	// Salir de la revision si cambio la tendencia
				$status = "canceled";
				$confirm['double-ema']['status'] = 0;
				$confirm['double-ema']['endTime'] = Carbon::now('UTC')->timestamp;
			}
		}
		// 2. Si stochastic es la primera en encontrase
		if (array_key_exists('stoch', $confirm) && $confirm['stoch']['position'] == '1') {
			$tp = Carbon::createFromTimestampUTC($confirm['stoch']['startTime'])->toDateTimeString();
			$dt = Carbon::now('UTC')->diffInMinutes($tp);
			$ticks = $spi->candlesticks($symbol, "5m",12);
			$oe = one_ma($ticks, 1, "assetVolume")['status'];
			$stoch = stochastic($chart);
			$stoch2 = stochastic($chart,2);
			$confirm['stoch']['values']['k'] = $stoch['values']['k'];
			$confirm['stoch']['values']['d'] = $stoch['values']['d'];
			// El stoch no haya superado los 5 minutos
			// $dt <= 60 && 

			if (($sar['status'] == 1 && $oe == 1 && $stoch2['status'] == 0) || rsi($chart, 25)['status'] == 1) {
				// Si es bajista esperar a que termine para entrar
				// Entrar y actualizar la base de datos
				// $per = -0.60;
				// $stop_loss = end($chart)['close'] + (end($chart)['close'] * ($per / 100));
				// $stop_loss = sprintf('%.8f', $stop_loss);
				$status = 'started';
			} elseif ($stoch['status'] == 0) {
				// Salir de la revision si cambio la tendencia
				$status = 'canceled';
				$confirm['stoch']['status'] = 3;
				$confirm['stoch']['endTime'] = Carbon::now('UTC')->timestamp;
			}
		}
		// 3. Si rsi-20 es la primera
		if (array_key_exists('rsi-20', $confirm) && $confirm['rsi-20']['position'] == '1') {
			$tp = Carbon::createFromTimestampUTC($confirm['rsi-20']['startTime'])->toDateTimeString();
			$dt = Carbon::now('UTC')->diffInMinutes($tp);

			// El rsi-30 no haya superado los 5 minutos
			// || $dt <= 60 && double_ema($chart) == 1
			$rsi_20 = rsi($chart, 20);
			$rsi_30 = rsi($chart, 30);
			if ($rsi_20['status'] == 1 || $rsi_30['status'] == 1) {
				// Entrar y actualizar la base de datos
				$status = 'started';
			} elseif ($dt >= 240 || rsi($chart, 30, 'above')['status'] == 1) {
				// Salir de la revision si cambio la tendencia
				$status = 'canceled';
				$confirm['rsi-20']['status'] = 0;
				$confirm['rsi-20']['endTime'] = Carbon::now('UTC')->timestamp;
			}
		}
		// 3. Si rsi-30 es la primera
		if (array_key_exists('rsi-30', $confirm) && $confirm['rsi-30']['position'] == '1') {
			$tp = Carbon::createFromTimestampUTC($confirm['rsi-30']['startTime'])->toDateTimeString();
			$dt = Carbon::now('UTC')->diffInMinutes($tp);

			// El rsi-30 no haya superado los 5 minutos
			// || $dt <= 60 && double_ema($chart) == 1
			$rsi_70 = rsi($chart, 70, "above");
			if ($dt <= 5 ) {
				// Entrar y actualizar la base de datos
				$status = 'started';
			} elseif ($dt >= 240 || $rsi_70['status'] == 1) {
				// Salir de la revision si cambio la tendencia
				$status = 'canceled';
				$confirm['rsi-30']['status'] = 0;
				$confirm['rsi-30']['endTime'] = Carbon::now('UTC')->timestamp;
			}
		}
		// 3. Si rsi-40 es la primera
		if (array_key_exists('rsi-40', $confirm) && $confirm['rsi-40']['position'] == '1') {
			$tp = Carbon::createFromTimestampUTC($confirm['rsi-40']['startTime'])->toDateTimeString();
			$dt = Carbon::now('UTC')->diffInHours($tp);

			// El rsi-40 no haya superado los 5 minutos
			$de = double_ema($chart);
			$rsi_50 = rsi($chart, 50, "above");
			$rsi_60 = rsi($chart, 60);
			$rsi_40 = rsi($chart, 40, "prev");
			$rsi_30 = rsi($chart, 30);
			if (($rsi_50['status'] == 1 && $rsi_60['status'] == 1 && $de == 2) || ($rsi_40['status'] == 1 && $de == 2) ) {
				// Entrar y actualizar la base de datos
				$status = 'started';
			} elseif ($dt > 4 && $rsi_40['status'] == 0 || $rsi_30['status'] == 1) {
				// Salir de la revision si cambio la tendencia
				$status = 'canceled';
				$confirm['rsi-40']['status'] = 0;
				$confirm['rsi-40']['endTime'] = Carbon::now('UTC')->timestamp;
			}
		}

		if($status != 'without') {
		 	if ($status == 'started' && $active && $order->btcAvailable()) {
		 		// ORDER BUY
	 			$response = $order->buy($close);

			 	if (!array_key_exists('code', $response)) {
			 		$data['orderBuy'] = $api->orderStatus($symbol, $response['orderId']);
			 		$data['amount'] = $response['origQty'];
			 		if ($data['orderBuy']['status'] != "FILLED") {
			 			$status = "review";
			 		} elseif ($data['orderBuy']['status'] == "FILLED") {
			 			$price['entry'] = $data['orderBuy']['price'];
						$price['entryTime'] = Carbon::now('UTC')->timestamp;
			 		}
			 	} else {
			 		// Informar al usuario del error
			 		$status = "review";
			 		$msg['errorOrder'] = $response;
			 		$msg = json_encode($msg);
			 		send_notification([
						"id" => $kcoin['id'],
						"title"	=> "Error con orden de compra: $symbol",
						"body" 	=> "{$response['code']}: {$response['msg']}",
						"status" => "error"
					]);
					print_r($response);
			 	}
		 	} 
		 	elseif ($active && !$order->btcAvailable()) {
		 		$status = "review";
		 	} else {
		 		$price['entry'] = $close;
				$price['entryTime'] = Carbon::now('UTC')->timestamp;
		 	}

		 	// Actualizar las alarmas

		 	DB::table("coin_review")->update([
				"status" => $status,
				"confirm" => json_encode($confirm),
				"price" => json_encode($price),
				"percentage" => json_encode($per),
				"stop_loss" => $stop_loss,
				"data" => json_encode($data),
				"msg" => $msg
			], "id='".$kcoin['id']."'");

		 	if ($status == "started") {
		 		send_notification([
					"id" => "{$kcoin['id']}",
					"title"	=> "Operación iniciada: $symbol",
					"body" 	=> "Nueva operación iniciada con {$price['entry']} BTC",
					"status" => "started"

				]);
				if ($alarmActived) {
			 		$cmd = "php /var/www/cyptobot/app/alarms.php";
			 		pids($cmd,'restart');
				}
		 	}
		 	
		 	echo "$symbol: $status\n";
		}
	}
	// 2.STARTED
	elseif ($kcoin['status'] == 'started') {
	// Evaluar para salir
		$status = $kcoin['status'];
		$per['last_price'] = percentage($price['entry'], $close);
		$per['high'] = percentage($price['entry'], $price['high']);
		$per['low'] = percentage($price['entry'], $price['low']);
		if ($active) {
			$orderBuyId = $data['orderBuy']['orderId'];
			$orderBuy = $api->orderStatus($symbol, $orderBuyId);
			if (array_key_exists('orderSell', $data)) {
				$data['orderSell'] = $api->orderStatus($symbol, $data['orderSell']['orderId']);
				if ($data['orderSell']['status'] == "FILLED") {
					$status = "finished";
					$price['exit'] = $data['orderSell']['price'];
					$price['exitTime'] = Carbon::now('UTC')->timestamp;
					$per['exit'] = percentage($price['entry'], $price['exit']);
					$balances = $api->balances($api->prices());

					$data['balances']['final'] = [
								'btc_available' => $balances['BTC']['available'],
								'btc_total' => $api->btc_value
							];
					send_notification([
						"id" => "{$kcoin['id']}",
						"title"	=> "Operacion finalizada $symbol (review)",
						"body" 	=> "Finalizada con ".truncateF($per['exit'],2)."%",
						"status" => "finished"
					]);
				}
				DB::table("coin_review")->update([
					"status" => $status,
					"price" => json_encode($price),
					"percentage" => json_encode($per),
					"data" => json_encode($data),
				], "id='{$kcoin['id']}'");

				if ($status == "finished") return false;
				// if ($data['orderType'] != 'stop_limit') return false;
			}
		}

		if (($active && $orderBuy['status'] == "FILLED") || !$active) {

			$review_status = 0;
			$type = "stop_limit";
			// $price = json_decode($kcoin['price'], true);
			$stop_loss = $kcoin['stop_loss'];

			$stoch = stochastic($chart);
			$stoch2 = stochastic($chart,2);

			$stoch_ex = array_key_exists('stoch', $confirm);
			$rsi40_ex = array_key_exists('rsi-40', $confirm);
			$rsi30_ex = array_key_exists('rsi-30', $confirm);
			$de_ex = array_key_exists('double-ema', $confirm);
			// PSAR este por debajo para actualizar el stop_loss
			if ($sar['status'] == 1) {
				$rsi_60 = rsi($chart, 60, "above");
				$de = double_ema($chart);
				// stop loss no sea 0 y menor al actual
				if (!$rsi40_ex && !$rsi30_ex && !$stoch_ex || ($rsi40_ex && $confirm['rsi-40']['position'] == '1' &&  $de == 1 && $rsi_60['status'] == 1) || ($stoch_ex && $confirm['stoch']['position'] == '1' && $stoch['status'] == 0) || ($rsi30_ex && rsi($chart, 40, "above")['status'] == 1 && $de == 1)) {
					$stop_loss = (!$sar['values'] || empty($sar['values'])) ? $stop_loss : $sar['values'];
					$per['stop_loss'] = percentage($price['entry'], $stop_loss);
					$review_status = 1;
				}
			}

			// Si Hay mas de 3,2,1% de ganancia actualizar stop_loss
			if (!$stoch_ex || ($stoch_ex && $stoch2['status'] == 1)) {
				if ($per['last_price'] > 1 /*&& $per['stop_loss'] < $per['last_price']*/) {
					// vender || stop-loss
					$stop_loss = $close;
					$per['stop_loss'] = percentage($price['entry'], $stop_loss);
					$type = "limit";
					$review_status = 1;
				}elseif (($per['last_price'] >= 0.50 || $per['last_price'] >= 0.25) /*&& $per['stop_loss'] < $per['last_price']*/ && ($rsi40_ex || $de_ex)) {
					
					$per['stop_loss'] = $per['last_price'] - 0.05;
					$stop_loss = $price['entry'] + ($price['entry'] * ($per['stop_loss'] / 100));
					$stop_loss = sprintf('%.8f', $stop_loss);
					$review_status = 1;
				}

			} elseif ($stoch_ex && $confirm['stoch']['position'] == 1 && $confirm['stoch']['status'] == 0) {
				// Si cambio la tendencia tratar de salir inmediatamente
				if ($per['last_price'] > 0.16) {
					$stop_loss = sprintf('%.8f', (($close * pow(10, 8)) - 1) / pow(10, 8));
					$per['stop_loss'] = percentage($price['entry'], $stop_loss);
					$type = "limit";
					$review_status = 1;
				}
			}

			if ($per['last_price'] > 3) {
				$stop_loss = sprintf('%.8f', (($close * pow(10, 8)) - 1) / pow(10, 8));
				$per['stop_loss'] = percentage($price['entry'], $stop_loss);
				$type = "limit";
				$review_status = 1;
			}

			if ($per['last_price'] > 0.20 && false) {
				$stop_loss = sprintf('%.8f', (($close * pow(10, 8)) - 1) / pow(10, 8));
				$per['stop_loss'] = percentage($price['entry'], $stop_loss);
				$type = "limit";
				$review_status = 1;
			}

			if ($review_status == 1 && $active) {
				//ORDER SELL
				$orderBuy = $api->orderStatus($symbol, $data['orderBuy']['orderId']);
				$response = $order->sell($stop_loss, $type, $orderBuy['origQty']);

			 	if (!array_key_exists('code', $response)) {
			 		$data = json_decode($kcoin['data'],true);
			 		$data['orderType'] = $type;
			 		$data['orderSell'] = $api->orderStatus($symbol, $response['orderId']);
			 		if ($data['orderSell']['status'] == 'FILLED') {
			 			$price['exit'] = $data['orderSell']['price'];
			 			$per['exit'] = percentage($price['entry'], $price['exit']);
			 			$status = "finished";
			 		}
			 		// $data = json_encode($data);
			 	} else {
			 		// Informar al usuario del error
		 			$msg = json_decode($kcoin['msg'],true);
		 			$msg['sell']['errorOrder'] = $response;
			 		$msg = json_encode($msg);
			 		send_notification([
						"id" => $kcoin['id'],
						"title"	=> "Error con orden de venta: $symbol",
						"body" 	=> "{$response['code']}: {$response['msg']}",
						"status" => "error"
					]);
					print_r($response);
			 	}
			}

			// actualizar confirmacion
			/*if (!$de_ex && double_ema($chart) == 1) {
				$confirm['double_ema']['startTimestamp'] = Carbon::now('UTC')->timestamp;
			} elseif ($de_ex && double_ema($chart) == 0) {
				$confirm['double_ema']['endTimestamp'] = Carbon::now('UTC')->timestamp;
			}*/

			if ($stoch_ex) {
				$stoch2 = stochastic($chart,2);
				// K Acaba de sobrepasar la linea 80
				if($stoch2['status'] == 1 && $confirm['stoch']['status'] == 1) {
					$confirm['stoch']['status'] = 2;
					$confirm['stoch']['values']['k'] = $stoch['values']['k'];
					$confirm['stoch']['values']['d'] = $stoch['values']['d'];
				}
				// K y D se cruzaron por arriba de 80
				if($stoch['status'] == 0 && $confirm['stoch']['status'] == 2) {
					$confirm['stoch']['status'] = 3;
					$confirm['stoch']['values']['k'] = $stoch['values']['k'];
					$confirm['stoch']['values']['d'] = $stoch['values']['d'];	
				}
				// K desciende y es menor a 80
				if($stoch2['status'] == 0 && ($confirm['stoch']['status'] == 2 || $confirm['stoch']['status'] == 3)) {
					$confirm['stoch']['status'] = 0;
					$confirm['stoch']['values']['k'] = $stoch['values']['k'];
					$confirm['stoch']['values']['d'] = $stoch['values']['d'];	
					$confirm['stoch']['endTime'] = Carbon::now('UTC')->timestamp;
				}
			}

			if ($close <= $stop_loss && !$active) {
				$price['exit'] = $stop_loss;
				$per['exit'] = percentage($price['entry'], $price['exit']);
				$status = "finished";
			}

			if ($status == "finished") {
				$price['exitTime'] = Carbon::now('UTC')->timestamp;
				$balances = $api->balances($api->prices());

				$data['balances']['final'] = [
					'btc_available' => $balances['BTC']['available'],
					'btc_total' => $api->btc_value
				];
				send_notification([
							"id" => "{$kcoin['id']}",
							"title"	=> "Operacion finalizada $symbol",
							"body" 	=> "Finalizada con ".truncateF($per['exit'],2)."% (review end)",
							"status" => "finished"
						]);
				if ($alarmActived) {
			 		$cmd = "php /var/www/cyptobot/app/alarms.php";
			 		pids($cmd,'restart');
				}
			}
			// $kcoin = DB::table("coin_review")->find($kcoin['id']);
			DB::table("coin_review")->update([
						"status" => $status,
						"confirm" => json_encode($confirm),
						"stop_loss" => $stop_loss,
						"percentage" => json_encode($per),
						"price" => json_encode($price),
						"data" => json_encode($data),
						"msg" => $msg
					], "id='".$kcoin['id']."'");

			echo "$symbol status:".$kcoin['status']." end\n";
		}
	}

	if (intval($options['activity']) == 1) {
		if ($options['id'] == $kcoin['id']) {
			send_notification([
				"activity" => "info",
				"id" => $options['id']
			]);
		}
	}

	$endTime = microtime(true);
	$time = $endTime - $startTime;
	echo "$time seconds\n";
	return false;
});
?>