<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app/orders.php';
require __DIR__ . '/../config/Process.php';
require __DIR__ . '/../indicators.php';

use Carbon\Carbon;

/*send_notification([
		"body" 	=> "El usuario envio comprobante de pago",
		"title"	=> "Verificacion de Orden"
	]);*/
$response = [];
$api =  new Binance\API(API_KEY,SECRET);
if (isset($_GET['op'])) {
	switch ($_GET['op']) {
		case 'getAll':
			$status = "status!='canceled'";
			$limit = "20";
			$c = 0;
			if (array_key_exists('canceled', $_GET) && boolval($_GET['canceled'])) {
				$status = "status='canceled'";
				$c++;
			}
			if (array_key_exists('review', $_GET) && boolval($_GET['review'])) {
				if ($c > 0) $status .= " || status='review'";
				else $status = "status='review'";
				$c++;
			}
			if (array_key_exists('started', $_GET) && boolval($_GET['started'])) {
				if ($c > 0) $status .= " || status='started'";
				else $status = "status='started'";
				$c++;
			}
			if (array_key_exists('finished', $_GET) && boolval($_GET['finished'])) {
				if ($c > 0) $status .= " || status='finished'";
				else $status = "status='finished'";
				$c++;
			}
			if (array_key_exists('limit', $_GET)) {
				$limit = $_GET['limit'];
			}
			$db = DB::table("coin_review")->select("*", "$status", "order by id desc limit $limit");
			$response['body'] = $db;
			$response['filter'] = $_GET;
			// $response['msg'] = "Success ;)";
			break;
		case 'filter':
			$query = "";
			if (!empty($_POST['status'])) {
				$status = $_POST['status'];
				$query .= "status='$status' ";
			}
			if (!empty($_POST['percentage'])) {
				$percentage = $_POST['percentage']; //">=9"
				$query .= "&& json_extract(percentage, '$.stop_loss') $percentage";
			}

			$db = DB::table("coin_review")->select("*", $query, "order by id desc");
			break;
		case 'edit':
			$_POST['id'];
			break;
		case 'getOnly':
			$id = $_GET['id'];
			$balances = DB::table("balances")->select("*","","ORDER BY id DESC LIMIT 1")[0];
			$db = DB::table("coin_review")->find($id);
			$data = json_decode($db['data'],true);
			$confirm = json_decode($db['confirm'], true);
			foreach($confirm as $kc => $vc) {
				foreach($vc as $in => $vin) {
					if (($in == 'startTime' || $in == 'endTime') && $vin != null) {
						$confirm[$kc][$in] = Carbon::createFromTimestamp($vin, 'America/Guayaquil')->toDateTimeString();
					} 
				} 
			}
			$db['confirm'] = $confirm;
			$price = json_decode($db['price'],true);
			$price['entryTime'] = isset($price['entryTime']) ? Carbon::createFromTimestamp($price['entryTime'], 'America/Guayaquil')->toDateTimeString() : '';
			$price['exitTime'] = isset($price['exitTime']) ? Carbon::createFromTimestamp($price['exitTime'], 'America/Guayaquil')->toDateTimeString() : '';
			$db['price'] = $price;
			$updated_at = ($db['status'] != 'canceled' && $db['status'] != 'finished') ? Carbon::now('America/Guayaquil') : $price['exitTime'];
			$db['diffTime']['create'] = convertSeconds(Carbon::parse($db['created_at'],'America/Guayaquil')->diffInSeconds($updated_at));
			$db['diffTime']['entry'] = convertSeconds(Carbon::parse($price['entryTime'],'America/Guayaquil')->diffInSeconds($updated_at));
			// $data['amount'] = empty($data['amount']) ? truncateF($api->balances()['BTC']['available'] / $price['last'],3) : $db['data']['amount'];
			$data['amount'] = array_key_exists('orderBuy', $data) ? $data['orderBuy']['origQty'] : $api->balances()['BTC']['available'];
			// $data['amount'] = $db['status'] == 'review' ? truncateF($api->balances()['BTC']['available'] / $price['last'],3) : $data['orderBuy']['origQty'];
			$db['data'] = $data;
			$response['body'] = $db;
			// $response['msg'] = "T r Success ;)";
			break;
		case 'setOrder':
			$tradingActived = (boolean) DB::table("settings")->find(3)['value'];
			$priceLimit = $priceStop = "";
			$id = $_GET['id'];
			$action = $_POST['action'];
			$db = DB::table("coin_review")->find($id);
			$symbol = $db['symbol'];
			$status = $db['status'];
			$rules = json_decode($db['rules'], true);
			$price = json_decode($db['price'], true);
			$per = json_decode($db['percentage'], true);
			$data = json_decode($db['data'], true);
			$msg = json_decode($db['msg'], true);
			$order = new Order($db['symbol'], $data);
			$rules[$action]['type'] = $type = $_POST['type'];
			if(isset($_POST['amount'])) $data['amount'] = $rules['amount'] = $_POST['amount'];
			unset($rules[$action]['percentage'], $rules[$action]['priceLimit'], $rules[$action]['priceStop'], $rules[$action]['conditions']);
			if(isset($_POST['percentage'])) $rules[$action]['percentage'] = $_POST['percentage'];
			if(isset($_POST['priceLimit'])) $priceLimit = $rules[$action]['priceLimit'] = $_POST['priceLimit'];
			if(isset($_POST['priceStop'])) $priceStop = $rules[$action]['priceStop'] = $_POST['priceStop'];
			if(isset($_POST['conditions'])) $rules[$action]['conditions'] = json_decode($_POST['conditions'], true);

			$response['msg'] = "Success";
			switch ($type) {
				case '0': //Limit Order
					$type = "limit";
					break;
				case '1': //Market Order
					$type = "market";
					break;
				case '2': //Stop-Limit buy=>Rule
					$type = $action == 'buy' ? "rule" : "stop_limit";
					break;
				case '3': //Rule
					$type = "rule";
					break;
			}
			if ($tradingActived) {
				//"Limit Order", "Market Order", "Stop-Limit", "Rule"
				$data['orderType'] = $type;
				// pids("php /var/www/cyptobot/app/review.php","stop");
				if ($action == 'buy' && $status == 'review'/* && $order->btcAvailable()*/ && $type != "stop_limit" && $type != "rule") {
			 		// ORDER BUY
		 			$result = $order->buy($_POST['priceLimit'], $type);
				 	if (!array_key_exists('code', $result)) {
				 		$data['orderBuy'] = $api->orderStatus($symbol, $result['orderId']);
				 		$data['amount'] = $rules['amount'] = $data['orderBuy']['origQty'];
				 		if ($data['orderBuy']['status'] == 'FILLED') $status = "started";
				 	} else {
				 		// Informar al usuario del error
				 		$status = "review";
				 		$msg['errorOrder'] = $result;
				 		$response['msg'] = "Error {$result['code']}";
				 	}
				} elseif ($action == 'sell' && $status == 'started' && $type != "rule") {
					// $orderBuy = $api->orderStatus($symbol, $data['orderBuy']['orderId']);
					// $quantity = (empty($data['amount']) || doubleval($data['amount']) <= 0) ? $orderBuy['origQty'] : $data['amount'];
					$result = $order->sell($priceLimit, $type, null, $priceStop);

				 	if (!array_key_exists('code', $result)) {
				 		// $data['orderSell'] = $api->orderStatus($symbol, $result['orderId']);
				 		$data['orderSell'] = $result;
				 		if ($data['orderSell']['status'] == 'FILLED') $status = "finished";
				 		// $data = json_encode($data);
				 	} else {
				 		// Informar al usuario del error
			 			$msg['sell']['errorOrder'] = $result;
				 		$response['msg'] = "Error {$result['code']} {$result['msg']}";
				 	}
				} else {
					$response['msg'] = "Error: No se puede ejecutar la orden";
				}
			} else {
				if ($action == "buy" && $type != "rule") {
					$status = "started";
				} elseif ($action == "sell" && $type != "rule") {
					$status = "finished";
				}
			}
			$price['last'] = $api->prices()[$db['symbol']];
			if ($action == "buy" && $status == "started") {
				$price['entry'] = $priceLimit;
				$price['entryTime'] = Carbon::now('UTC')->timestamp;
			} elseif ($action == "sell" && $status == "finished") {
				$price['exit'] = $priceLimit;
				$price['exitTime'] = Carbon::now('UTC')->timestamp;
				$per['last_price'] = percentage($price['entry'], $price['last']);
				$per['exit'] = percentage($price['entry'], $priceLimit);
			}
			
			$db = DB::table("coin_review")->update([
				"status" => $status,
				"rules" => json_encode($rules),
				"price" => json_encode($price),
				"percentage" => json_encode($per),
				"data" => json_encode($data),
				"msg" => json_encode($msg)
			],"id='$id'");
			// pids("php /var/www/cyptobot/app/review.php","restart"," --activity=0");
			break;
		case 'getHome':
			$dateNow = Carbon::now('America/Guayaquil')->toDateString();
			$db = DB::table("coin_review")->select("*", "status='finished' AND DATE_FORMAT(created_at, '%m-%d') = DATE_FORMAT('$dateNow', '%m-%d')");
			$balances = DB::table("balances")->select("*","","ORDER BY id DESC LIMIT 1");
			$profitNumber = 0;
			$profitPer = 0;
			$lossPer = 0;
			$lossNumber = 0;
			
			foreach ($db as $coin) {
				$per = json_decode($coin['percentage'], true);
				if ($per['exit'] > 0) {
					$profitNumber++;
					$profitPer += $per['exit'];
				} elseif ($per['exit'] < 0) {
					$lossNumber++;
					$lossPer += $per['exit'];
				}
			}

			$response['body']['profit'] = ["operations" => "$profitNumber", "percentage" => "$profitPer"];
			$response['body']['loss'] = ["operations" => "$lossNumber", "percentage" => "$lossPer"];
			$response['body']['balances'] = $balances[0];
			// $response['body']['total'] = ["operations" => "$lossNumber ", "percentage" => ""];
			break;
		case 'getSetting':
			$db = DB::table("settings")->select("*");
			$response['body'] = $db;
			break;
		case 'setSetting':
			$id = $_GET['id'];
			// $key = $_POST['key'];
			$value = $_POST['value'];

			$cmd = "php /var/www/cyptobot/app/review.php";
			
			if ($id == "2") {
				$cmd = "php /var/www/cyptobot/app/alarms.php";
			}
			if ($id == "3" || $id == "4") {
				pids("php /var/www/cyptobot/app/app2.php", "restart", " --activity=0");
				pids("php /var/www/cyptobot/app/review.php", "restart", " --activity=0");
				$response['msg'] = "Success";
			}

			$actived = DB::table("settings")->find($id);
			if ($id != "3" && $id != "4") {
				if (boolval($value))
					$response['msg'] = pids($cmd,"verify", " --activity=0");
				else
					$response['msg'] = pids($cmd,'stop');
			}

			DB::table("settings")->update([
				"value" => $value
			],"id='$id'");
			break;
		case 'alarm':
			if (array_key_exists("id", $_GET)) $id = $_GET['id'];
			$values = array_key_exists("values", $_POST) ? $_POST["values"] : NULL;
			$alarm = DB::table("alarms");
			$exec = 0;
			switch ($_POST['action']) {
				case 'create':
					$result = $alarm->insert([
						"coin" => $_POST["coin"],
						"active" => 1,
						"indicator" => $_POST["indicator"],
						"rule" => $_POST["condition"],
						"vals" => $values
					]);
					$response['body'] = $result['id'];
					$exec = 1;
					break;
				case 'read':
					$db = $alarm->select("*");
					$symbols = DB::table("symbols")->select("name","","order by name asc");
					$response['body'] = $db;
					$response['symbols'] = array_column($symbols, "name");
					break;
				case 'edit':
					$getAlarm = $alarm->find($id);
					$coin = array_key_exists("coin", $_POST) ? $_POST["coin"] : $getAlarm['coin'];
					$active = array_key_exists("active", $_POST) ? $_POST["active"] : $getAlarm['active'];
					$indicator = array_key_exists("indicator", $_POST) ? $_POST["indicator"] : $getAlarm['indicator'];
					$rule = array_key_exists("rule", $_POST) ? $_POST["condition"] : $getAlarm['rule'];
					$vals = array_key_exists("values", $_POST) ? $values : $getAlarm['vals'];
					
					$alarm->update([
						'coin' => $coin,
						'active' => $active,
						'indicator' => $indicator,
						'rule' => $rule,
						'vals' => $vals
					],"id='$id'");
					$exec = 1;
					$response['msg'] = "Success $id act:{$_POST["active"]}";
					break;
				case 'delete':
					$alarm->delete("id='$id'");
					$exec = 1;
					break;
				default:
					break;
			}
			if ($exec == 1) pids("php /var/www/cyptobot/app/alarms.php","restart");
			break;
		case 'getHistory':
			$dateNow = Carbon::now('America/Guayaquil')->toDateString();
			$db = DB::table("coin_review")->select("*", "status='finished' AND DATE_FORMAT(created_at, '%m') = DATE_FORMAT('$dateNow', '%m')", "ORDER BY created_at DESC");
			$list = [];

			// $dateDaily = Carbon::parse(end($db)['updated_at'])->toDateString();
			$dateDaily = array_column($db, 'created_at');

			// $allsDays = array($dateDaily);
			foreach ($dateDaily as $key => $coin) {
				$dateDaily[$key] = Carbon::parse($coin)->toDateString();
			}

			$profitNumber = 0;
			$profitPer = 0;
			$lossPer = 0;
			$lossNumber = 0;
			$i = 0;
			$month['profit']['percentage'] = $month['profit']['operations'] = $month['loss']['percentage'] = $month['loss']['operations'] = $month['total']['operations'] = $month['total']['percentage'] = 0;
			$dateDaily = array_unique($dateDaily);
			foreach ($dateDaily as $day) {
				$list[$i]['date'] = $day;
				foreach ($db as $coin) {
					$per = json_decode($coin['percentage'],true);
					$updated_at = Carbon::parse($coin['created_at'])->toDateString();
					if ($day == $updated_at) {
						if ($per['exit'] > 0) {
							$profitNumber++;
							$profitPer += $per['exit'];
						} elseif ($per['exit'] < 0) {
							$lossNumber++;
							$lossPer += $per['exit'];
						}
						$list[$i]['profit']['percentage'] = $profitPer;
						$list[$i]['profit']['operations'] = $profitNumber;
						$list[$i]['loss']['percentage'] = $lossPer;
						$list[$i]['loss']['operations'] = $lossNumber;
						$list[$i]['total']['percentage'] = $profitPer + $lossPer;
						$list[$i]['total']['operations'] = $profitNumber + $lossNumber;
					} else {
						$profitNumber = 0;
						$profitPer = 0;
						$lossPer = 0;
						$lossNumber = 0;
					}
				}
				if ($i > 0) {
					$month['profit']['percentage'] += $list[$i]['profit']['percentage'];
					$month['profit']['operations'] += $list[$i]['profit']['operations'];
					$month['loss']['percentage'] += $list[$i]['loss']['percentage'];
					$month['loss']['operations'] += $list[$i]['loss']['operations'];
					$month['total']['percentage'] += $list[$i]['total']['percentage'];
					$month['total']['operations'] += $list[$i]['total']['operations'];
				}
				$i++;
			}
			// asort($dateDaily);
			$response['month'] = $month;
			$response['countDaily'] = count($dateDaily);
			$response['body'] = $list;
			/*$response['count'] = count($db);
			$response['body'] = $db;
			$profitNumber = 0;
			$profitPer = 0;
			$lossPer = 0;
			$lossNumber = 0;
			
			continue;
			$list = [];
			foreach ($db as $coin) {
				if ($dateDaily == $coin['updated_at']) {
					
				}
				$per = json_decode($coin['percentage'], true);
				if ($per['stop_loss'] > 0) {
					$profitNumber++;
					$profitPer += $per['stop_loss'];
				} elseif ($per['stop_loss'] < 0) {
					$lossNumber++;
					$lossPer += $per['stop_loss'];
				}
			}

			$response['body']['profit'] = ["operations" => "$profitNumber", "percentage" => "$profitPer"];
			$response['body']['loss'] = ["operations" => "$lossNumber", "percentage" => "$lossPer"];
			$response['body']['balances'] = $balances[0];*/
			// $response['body']['total'] = ["operations" => "$lossNumber ", "percentage" => ""];
			break;

			case 'getBlock':
				$locked = DB::table('symbols')->select("name,price","locked=1","order by name asc");
				$db = DB::table('symbols')->select("name,price","","order by name asc");
				$symbols = [];
				foreach ($db as $symbol) $symbols[$symbol['name']] = $symbol['price'];
				$response['body'] = $locked;
				$response['symbols'] = $symbols;
				break;

			case 'setBlock':
				$action = $_POST['action'];
				if ($action == 'block')
					$locked = 1;
				elseif ($action == 'delete') {
					$locked = 0;
				}
				DB::table('symbols')->update(["locked" => $locked],"name='{$_POST['symbol']}'");

				break;

			case 'reviewCancel':
				$coin = DB::table('coin_review')->find($_GET['id']);
				if ($coin['status'] == 'review') {
					DB::table('coin_review')->update(["status" => "canceled"],"id='{$_GET['id']}'");
					if ($_POST['block'] == '1') {
						DB::table('symbols')->update(["locked" => intval($_POST['block'])],"name='{$_POST['symbol']}'");
					}
				} else {
					$response['msg'] = "Ya ha sido cancelada";
				}
				break;

			case 'addCoin':
				$cmd = "php /var/www/cyptobot/app/review.php";
				$symbol = $_POST['symbol'];
				$tbCoin = DB::table('coin_review');
				$coinsActived = $tbCoin->select("symbol,status","(status='review' || status='started') && symbol='$symbol'");

				if (count($coinsActived) > 0) {
					$response['msg'] = "Hay una operación activa";
				} else {
					$price = ["last" => $api->prices()[$symbol]];
					$balances = $api->balances($api->prices());
					$data = [
						'balances' => [
							'start' => [
								'btc_available' => $balances['BTC']['available'],
								'btc_total' => $api->btc_value
							]
						]
					];
					$result = $tbCoin->insert([
						"symbol" => $symbol,
						"status" => "review",
						"priority" => "1",
						"confirm" => "{}",
						"price" => json_encode($price),
						"data" => json_encode($data)
					]);
					$reviewActived = DB::table("settings")->find(1);
					if (boolval($reviewActived['value'])) pids($cmd,'restart'," --activity=0");
					$response['body'] = $result;
				}
				break;

			case 'openOrders':
				if (array_key_exists('symbol', $_GET)) {
					$openorders = $api->openOrders($_GET['symbol']);
					foreach ($openorders as $keyO => $valueO) {
						$openorders[$keyO]['time'] = Carbon::createFromTimestamp($valueO['time']/1000, 'America/Guayaquil')->toDateTimeString();
						$openorders[$keyO]['updateTime'] = Carbon::createFromTimestamp($valueO['updateTime']/1000, 'America/Guayaquil')->toDateTimeString();
						$openorders[$keyO]['elapsed'] = convertSeconds(Carbon::now('America/Guayaquil')->diffInSeconds($openorders[$keyO]['time']));
					}
					$response['body'] = $openorders;
				}
				break;
			case 'realtime':
				// info home review
				if (array_key_exists('activity', $_GET) && array_key_exists('option', $_GET)) {
					$cmd = "";
					$id = "";
					$option = "--activity=0";
					switch ($_GET['activity']) {
						case 'home':
							$cmd = "php /var/www/cyptobot/app/balances.php";
							break;

						case 'review':
							$cmd = "php /var/www/cyptobot/app2.php";
							break;
	
						case 'info':
							$cmd = "php /var/www/cyptobot/app/review.php";
							if ($_GET['option'] == '1') $id = " --id={$_GET["id"]}";
							break;
					}
					$status = "restart";
					if ($_GET['option'] == '1'){
						$option = "--activity=1$id";
					}  else {
						$status = "stop";
					}
					if (!empty($cmd)) pids($cmd, $status, $option);
				}

				break;
			case 'getJson':
				$response['body'] = truncateF(1204.992,2);

				break;
				/*
				$x = 200;
				for ($i=0; $i < 300 ; $i++) { 
					$x = $x * 1.01;
				}*/
				/*$ps = DB::table("process")->select("*", "status='1'");
				foreach ($ps as $pss) {
					$process = new Process();
        			$process->setPid($pss['pid']);
        			$process->stop();
    				// if (!$process->status()) {
    					DB::table("process")->update([
			                "status" => 0,
			            ], "id='".$pss['id']."'");
    				// }
				}*/
				// $price = "0.00332520" / "0.00000255";
				// $vist = truncateF($price,0);
				// $order = new Order("LINKBTC");
				// $price = '0.00050770' - ('0.00050770' * 0.15);
				// $response['body'] = $price;
				// $result = $order->buy("0.00041520", "limit");
				// $result = $order->buy("0", "limit");


				$orders = $api->orderStatus("WTCBTC", "35277932");
				// print_r($orders);
				$time = Carbon::createFromTimestamp(1535239999874/1000, 'America/Guayaquil')->toDateTimeString();
				$seconds = Carbon::now('America/Guayaquil')->diffInSeconds($time);

				$response['body'] = $orders;
				$response['time'] = $time;
				$response['seconds'] = $seconds;
				// $response['body']['price'] = $price;
				break;
		default:
			$response = 0;
			break;
	}

	if ($response != 0) {
		$response = json_encode($response);
		print($response);
	}
}

function convertSeconds($secondsTime) {
    $hours = floor($secondsTime / 3600);
    $minutes = floor(($secondsTime - ($hours * 3600)) / 60);
    $seconds = $secondsTime - ($hours * 3600) - ($minutes * 60);

    $full_time = "";
    if ($hours > 0 ) {
        $full_time .= $hours . "h:";
    }

    if ($minutes > 0 ) {
        $full_time .= $minutes . "m:";
    }

    if ($seconds > 0 ) {
        $full_time .= $seconds . "s";
    }

    return $full_time;
}
?>