<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/app/orders.php';
require_once __DIR__ . '/config/Process.php';
use Carbon\Carbon;

$timeCurrent = Carbon::now('America/Guayaquil');
echo "$timeCurrent".PHP_EOL;
while(Carbon::now('America/Guayaquil')->diffInSeconds($timeCurrent) < 59) {
	$alarmActived = (boolean) DB::table("settings")->find(2)['value'];
	$testActive = (boolean) DB::table("settings")->find(4)['value'];
	$tradingActive = (boolean) DB::table("settings")->find(3)['value'];

	$cmd = "php ".__DIR__."/app2.php";
	if ($testActive || $tradingActive) {
		deleteDuplicate($cmd);
		echo "App2: ".pids($cmd,"verify"," --activity=0")."\n";
	} else {
		deleteDuplicate($cmd);
		echo "App2: ".pids($cmd,"stop")."\n";
	}

	$cmd = "php ".__DIR__."/app/alarms.php";
	if ($alarmActived) {
		deleteDuplicate($cmd);
		echo "Alarms: ".pids($cmd)."\n";
	} else {
		pids($cmd, 'stop');
	}

	$cmd = "php ".__DIR__."/app/balances.php";
	deleteDuplicate($cmd);
	pids($cmd,"verify"," --activity=0");
	pids("php ".__DIR__."/app/user_data.php", "verify");
	$ps = DB::table("process")->select("*", "status='1'");
	foreach ($ps as $pss) {
		$process = new Process();
		$process->setPid($pss['pid']);
		if (!$process->status()) {
			DB::table("process")->update([
	            "status" => 0,
	        ], "id='".$pss['id']."'");
		}
	}

	deleteDuplicate("php ".__DIR__."/app/review.php");
	DB::table("process")->delete("status='0'");
	
	if ($tradingActive) {
		$coinStart = DB::table("coin_review")->select("*", "status='started'");
		if (count($coinStart) > 0) {
			$coinReview = DB::table("coin_review")->select("*", "status='review'");
			foreach($coinReview as $op) {
				$createDateTime = Carbon::now('America/Guayaquil')->diffInSeconds($op['created_at']);
				if ($createDateTime > 300) DB::table("coin_review")->update(["status" => "canceled"],"id='{$op['id']}'");
			} 
		} 
		
	} 
}
pids("php ".__DIR__."/app/user_data.php", "restart");
echo Carbon::now('America/Guayaquil')." end".PHP_EOL;
?>