<?php
require_once __DIR__ . '/vendor/autoload.php';
use Carbon\Carbon;
/*
1.doble ema, estocastico y rsi para entradas

2.estocastico, bandas de bollinger y rsi para salidas
	*asegurar 1% si es posible

3.psar para stop-loss dinamicos en las entradas
*/
ini_set('trader.real_precision', '8');

function one_ma($ticks/*, $api, $symbol, $interval = "1d"*/, $option, $key = "close", $timePeriod = 10)
{
	$r = -1;
	// $ticks =  $api->candlesticks($symbol, $interval);
	$ma = trader_ma(array_column($ticks, $key), $timePeriod, TRADER_MA_TYPE_SMA);

	if ($ma) {
		$i = 0;
		foreach ($ticks as $kt => $kv) {
			$ticks[$kt]['ma'] = isset($ma[$i]) ? sprintf('%.8f', $ma[$i]) : '';
			$i++;
		}

		$endTicks = end($ticks);
		$prevTicks = prev($ticks);
		switch ($option) {
			case 1:
				//la barra actual es mayor al ma
				if ($endTicks[$key] > $endTicks['ma']) $r = 1;
				else $r = 0; 
				break;
			case 2:
				//la barra actual y la anterior son mayores al ma
				if ($endTicks[$key] > $endTicks['ma'] && $prevTicks[$key] > $prevTicks['ma']) $r = 1;
				else $r = 0;
				break;
			case 3:
				//la barra actual es menor al ma
				if ($endTicks[$key] < $endTicks['ma']) $r = 1;
				else $r = 0;
				break;
			case 4:
				//la barra actual y la anterior son menores al ma
				if ($endTicks[$key] < $endTicks['ma'] && $prevTicks[$key] < $prevTicks['ma']) $r = 1;
				else $r = 0;
				break;
			default:
				$r = 0;
				break;
		}
		
		return ['status' => $r, 'values' => $ma];
	}
	else
		return $r;
}

function one_ema($api, $symbol, $interval = "4h", $timePeriod = 200, $key = "close")
{
	$r = -1;
	$ticks =  $api->candlesticks($symbol, $interval);
	$ema200 = trader_ema(array_column($ticks, $key), $timePeriod);

	if ($ema200) {
		$i = 0;
		foreach ($ticks as $kt => $kv) {
			$ticks[$kt]['ema200'] = isset($ema200[$i]) ? sprintf('%.8f', $ema200[$i]) : '';
			$i++;
		}

		$endTicks = end($ticks);
		if ($endTicks[$key] > $endTicks['ema200']) $r = 1;
		else $r = 0;

		return ['status' => $r, 'values' => $ema200];
	}
	else
		return $r;

}

function double_ema($ticks, $fastPeriod = 9, $slowPeriod = 21)
{
	$x = -1;
	$fastEma = trader_ema(array_column($ticks, 'close'), $fastPeriod);
	$slowEma = trader_ema(array_column($ticks, 'close'), $slowPeriod);
	
	if(!$fastEma || !$slowEma) return -2;
	
	$endFastEma = end($fastEma);
	$prevFastEma = prev($fastEma);
	$secFastEma = prev($fastEma);
	$endSlowEma = end($slowEma);
	$prevSlowEma = prev($slowEma);
	$secSlowEma = prev($slowEma);

	if ($endFastEma > $endSlowEma && $endFastEma > $prevFastEma) $x = 1;
	if ($endFastEma > $endSlowEma /*&& $prevFastEma > $endSlowEma*/ && $secFastEma < $secSlowEma /*&& $endFastEma > $prevFastEma*/) $x = 2;
	if ($endFastEma < $endSlowEma) $x = 0;

	return $x;
}

function rsi($ticks, $value, $query = "below", $timePeriod = 14)
{
	$x = 0;

	$rsi = trader_rsi(array_column($ticks, 'close'), $timePeriod);

	if (!$rsi) return -1;
	$end_rsi = end($rsi);
	$prev_rsi = prev($rsi);
	$sec_rsi = prev($rsi);
	
	if ($end_rsi > 0) {
		switch ($query) {
			case 'below':
				if ($end_rsi < $value) $x = 1;
				break;
			case 'above':
				if ($end_rsi > $value) $x = 1;
				break;
			case 'cross':
				if ($prev_rsi < $value && $end_rsi > $value) $x = 1;
				break;
			case 'prev':
				if ($sec_rsi < $end_rsi && $end_rsi >= $value) $x = 1;
				break;
			default:
				$x = 0;
				break;
		}
	}

	return ['status' => $x, 'values' => $rsi];
}

function stochastic($ticks, $option = 1, $fastK_period = 14, $slowD_period = 3, $smooth = 3)
{
	$r = -1;
	$stoch = trader_stoch(array_column($ticks, 'high'), array_column($ticks, 'low'), array_column($ticks, 'close'), $fastK_period, $smooth, TRADER_MA_TYPE_SMA, $slowD_period, TRADER_MA_TYPE_SMA);

	if (!$stoch) return -1;

	$i = 0;
	foreach ($ticks as $kt => $kv) {
		$ticks[$kt]['k'] = isset($stoch[0][$i]) ? $stoch[0][$i] : '';
		$ticks[$kt]['d'] = isset($stoch[1][$i]) ? $stoch[1][$i] : '';
		$i++;
	}

	$endTicks = end($ticks);
	$prevTicks = prev($ticks);

	switch ($option) {
		case 1:
			// Cruzaron por debajo de 20
			if ($endTicks['k'] > $endTicks['d'] && $endTicks['k'] >= $prevTicks['k'] && $endTicks['d'] <= 20.0000 && $endTicks['k'] >= 14) $r = 1;
			// Cruzaron por encima de 80
			elseif ($endTicks['k'] < $endTicks['d'] && $endTicks['d'] <= $prevTicks['d'] && $endTicks['d'] >= 80) $r = 0;
			break;
		case 2:
			if ($endTicks['k'] > 80) $r = 1;
			elseif ($endTicks['k'] < 80) $r = 0;
			break;
		case 3:
			if ($endTicks['k'] < $prevTicks['k'] && $endTicks['k'] >= 80) $r = 1;
			else $r = 0;
			break;
		default:
			$r = -1;
			break;
	}
	
	return ['status' => $r, 'values' => $endTicks];
}

function macd($ticks, $fastPeriod = 12, $slowPeriod = 26, $signalPeriod = 9)
{
	$coin = [];

	$macd = trader_macd(array_column($ticks, 'close'), $fastPeriod, $slowPeriod, $signalPeriod);

	return $macd;
}

function bollinger($ticks, $timePeriod = 20)
{
	$coin = [];
	foreach ($ticks as $kts => $vts) {
		array_push($coin, $vts);
		$ticks[$kts]['close'] = $ticks[$kts]['close'] * 100000000;
	}

	$bb = trader_bbands(array_column($ticks, 'close'), $timePeriod, 2.0, 2.0, TRADER_MA_TYPE_SMA);

	foreach ($coin as $kc => $vc) {
		$coin[$kc]['bb_upper'] = isset($bb[0][$kc]) ? sprintf('%.8f', $bb[0][$kc] / 100000000) : '';
		$coin[$kc]['bb_middle'] = isset($bb[1][$kc]) ? sprintf('%.8f', $bb[1][$kc] / 100000000) : '';
		$coin[$kc]['bb_lower'] = isset($bb[2][$kc]) ? sprintf('%.8f', $bb[2][$kc] / 100000000) : '';
	}

	/*if ($) {
	}*/
	return $coin;

}

//limit 1d:23
// acceleration 1d:0.02136
function sar($ticks, $acceleration = 0.02, $maximun = 0.2)
{
	$r = -1;
	$psar = trader_sar(array_column($ticks, 'high'), array_column($ticks, 'low'), $acceleration, $maximun);

	if (!$psar) return $r;

	$i = 0;
	foreach ($ticks as $k => $v) {
		$ticks[$k]['psar'] = isset($psar[$i]) ? sprintf('%.8f', $psar[$i]) : '';
		$i++;
	}

	$endTicks = end($ticks);
	if (floatval($endTicks['psar']) <  floatval($endTicks['close']))
		$r = 1;
	elseif (floatval($endTicks['psar']) >  floatval($endTicks['close']))
		$r = 0;

	return ['status' => $r, 'values' => sprintf('%.8f', end($psar))];
}

function percentage($x, $y)
{
	return (($y - $x) * 100) / $x;
}

function candle($chart)
{
	$endC = end($chart);
	// $prevC = prev($chart);

	$ot = Carbon::createFromTimestampUTC((int)($endC['openTime']/1000));
	// $ct = Carbon::createFromTimestampUTC((int)($endC['closeTime']/1000));
	$nw = Carbon::now('UTC');
	$diff = $nw->diffInSeconds($ot);
	// $diffoc = $ct->diffInSeconds($ot);

	if ($diff >= 150) {
		if ($endC['close'] < $endC['open']) return 1; // vela bajista
		else return 0; // vela alcista
	}
}