<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/indicators.php';
require_once __DIR__ . '/config/Process.php';
require __DIR__ . '/app/orders.php';

use Carbon\Carbon;

$api = new Binance\API(API_KEY,SECRET);
$cd = new Binance\API(API_KEY,SECRET);

// 0.00295409 BTC | 0.00399537
$argv = array_slice($argv, 1);

$api->chart($argv, "5m", function($api, $symbol, $chart) {
	
	/*$getOrders = $api->orders($symbol);
	$buyOrders = [];
	foreach ($getOrders as $order) {
		if ($order['side'] == 'BUY' && $order['status'] == 'FILLED') {
			array_push($buyOrders, $order);
		}
	}*/
	// $price['entry'] = end($buyOrders)['price'];
	$price['entry'] = "0.0003490";
	// print_r($getOrders);
	// exit();
	$close = end($chart)['close'];
	$review_status = 0;
	$order = new Order($symbol);
	if ($price['entry'] > 0) {
		$per_close = percentage($price['entry'], $close);
	} else 
	 	return false;
	// Si Hay mas de 3,2,1% de ganancia actualizar stop_loss
	if ($per_close > 3) {
		$stop_loss = $close;
		$per = percentage($price['entry'], $close);
		$review_status = 1;
	}elseif ($per_close > 2) {
		$stop_loss = $close;
		$per = percentage($price['entry'], $close);
		$review_status = 1;
	}elseif ($per_close > 1) {
		$stop_loss = $close;
		$per = percentage($price['entry'], $close);
		$review_status = 1;
	}elseif ($per_close > 0.90 || $per_close > 0.60 || $per_close >= 0.50) {
		$stop_loss = $close;
		$per = percentage($price['entry'], $close);
		$review_status = 1;
	}

	if ($review_status == 1) {
		// echo "$symbol: vendido en $stop_loss con procentaje del $per \n";
		$response = $order->sell($stop_loss,"LIMIT");
		if (!array_key_exists('code', $response)) {
			echo "$symbol: vendido en $stop_loss con procentaje del $per | order ID:".$response['orderId']."\n";
			exit();
		} else {
			print_r($response);
		}
	}

	echo "$symbol: $close con procentaje del $per_close \n";
});
?>