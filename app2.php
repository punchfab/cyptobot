<?php
require __DIR__ . '/vendor/autoload.php';
require __DIR__ . '/indicators.php';
require_once __DIR__ . '/config/Process.php';
require __DIR__ . '/app/orders.php';

use Carbon\Carbon;

$api =  new Binance\API(API_KEY,SECRET);
$cmd = "php ".__DIR__."/app/review.php";
$longopts = array(
	"activity:",
);
$options = getopt("", $longopts);
// 1. Verificar las monedas
$api->ticker(false, function($api, $symbol, $ticker) {
	global $cmd;
	global $options;
	$active = (boolean) DB::table("settings")->find(3)['value'];
	$testActive = (boolean) DB::table("settings")->find(4)['value'];
	if (!$active && !$testActive) exit();
	$order = new Order($symbol);
	$offset = strlen($ticker['symbol']) - 3;
	$oticks =  $api->candlesticks($ticker['symbol'], "5m");
	$tickDay = $api->candlesticks($ticker['symbol'], "1d", 1);
	$volume =  end($tickDay)['assetVolume'];
	$coin_review = DB::table("coin_review");
	$tbProcess = DB::table("process");
	$reviewActived = DB::table("settings")->find(1);
    $ps = $tbProcess->select("*", "cmd='$cmd' && status='1'");
    $balances = $api->balances($api->prices());
    $symbolsLocked = array_column(DB::table("symbols")->select("name","locked=1"), "name");

	// Verficar si la moneda esta en revision
	$result = count($coin_review->select('*',"symbol='$symbol' && status!='finished' && status!='canceled'"));
	if(/*floatval($ticker['percentChange']) > 1 &&*/!in_array($ticker['symbol'], $symbolsLocked) && strpos($ticker['symbol'], 'BTC', $offset) !== false && strpos($ticker['symbol'], 'USD') === false  && $volume >= 100000){
		// echo $ticker['symbol']." change%: ".$ticker['percentChange']." volume: ".$ticker['volume']."\n";
    	// echo $ticker['symbol'] . "\n";

		$ema_l = one_ema($api, $ticker['symbol']);
		if ($ema_l['status'] == 1)
		{
			$stoch = stochastic($oticks);
			$rsi_40 = rsi($oticks, 40, "cross");
			$rsi_30 = rsi($oticks, 30, "cross");
			$rsi_20 = rsi($oticks, 20);
			$priori = $position = 0;
			$confirm = [];
			$status = 'review';

			if ($rsi_40['status'] == 1) {
				$priori += 2;
				$position += 1;
				$confirm['rsi-40'] = [
						'position' => $position,
						'status' => 1,
						'startTime' => Carbon::now('UTC')->timestamp,
						'endTime' => Null,
						'values' => end($rsi_40['values'])
					];
			}elseif ($rsi_30['status'] == 1) {
				$priori += 3;
				$position += 1;
				$confirm['rsi-30'] = [
						'position' => $position,
						'status' => 1,
						'startTime' => Carbon::now('UTC')->timestamp,
						'endTime' => Null,
						'values' => end($rsi_30['values'])
					];
			}elseif ($rsi_20['status'] == 1) {
				$priori += 3;
				$position += 1;
				$confirm['rsi-20'] = [
						'position' => $position,
						'status' => 1,
						'startTime' => Carbon::now('UTC')->timestamp,
						'endTime' => Null,
						'values' => end($rsi_20['values'])
					];
			}

			if ($stoch['status'] == 1) {
				$priori += 2;
				$position += 1;
				$confirm['stoch'] = [
						'position' => $position,
						'status' => 1,
						'startTime' => Carbon::now('UTC')->timestamp,
						'endTime' => Null,
						'values' => ['k' => $stoch['values']['k'], 'd' => $stoch['values']['d']]
					];
			}

			if (double_ema($oticks) == 1) {
				$priori += 1;
				$position += 1;
				$confirm['double-ema'] = [
						'position' => $position,
						'status' => 1,
						'startTime' => Carbon::now('UTC')->timestamp,
						'endTime' => Null,
					];

			}

			if ($priori > 0) {

				// $conf = implode(", ", $confirm);
				echo $ticker['symbol']." change%: ".$ticker['percentChange']." priori:$priori result:$result\n";
				// Ingresa la moneda a revision
				$price = ['review' => $ticker['close'], 'last' => $ticker['close'], 'high' => $ticker['close'], 'low' => $ticker['close']];
				$percentage = ['review' => 0, 'stop_loss' => 0, 'last_price' => 0, 'exit' => 0, 'high' => 0, 'low' => 0];
				$data = ['balances' => ['start' => [
							'btc_available' => $balances['BTC']['available'],
							'btc_total' => $api->btc_value
						]]];
				$tstart = $coin_review->select("*", "status='started'");
				if (($result < 1 && !$active) || ($result < 1 && $active && /*$order->btcAvailable() && */ count($tstart) < 1)) {
					$confirm = json_encode($confirm);
					$insert = $coin_review->insert([
						"symbol" => $symbol,
						"status" => $status,
						"priority" => $priori,
						"confirm" => $confirm,
						"price" => json_encode($price),
						"percentage" => json_encode($percentage),
						"data" => json_encode($data)
					]);

					send_notification([
							"id" => "{$insert['id']}",
							"body" 	=> "$symbol entro en revisión",
							"title"	=> "Nueva operación en revisión",
							"status" => "review"
						]);
					

					// if (boolval($reviewActived))
					if (boolval($reviewActived['value'])) {
						pids($cmd,'restart',"--activity=0");
					} else {
						pids($cmd,'stop');
					}
				}
			}
			if ($result > 0 && boolval($reviewActived['value'])) pids($cmd,"verify","--activity=0"); //Si no hay niguna entrada nueva analiza que el proceso siga activo
			elseif (!boolval($reviewActived['value'])) {
				// Existe el proceso activo
				pids($cmd,'stop');
			}
		}
		if (intval($options['activity']) == 1) {
			send_notification([
				"activity" => "review"
			]);
		}
	}
	echo boolval($reviewActived['value'])."\n";
});
?>